﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess;

public static class MongoServiceExtensions
{
    public static IServiceCollection ConfigureService(this IServiceCollection services)
    {
        var pack = new ConventionPack
        {
            new CamelCaseElementNameConvention(),
        };

        ConventionRegistry.Register("CamelCase", pack, t => true);

        BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));

        RegisterClassMaps();

        return services;
    }

    private static void RegisterClassMaps()
    {
        BsonClassMap.RegisterClassMap<PromoCode>(map =>
        {
            map.AutoMap();

            map.MapMember(emp => emp.PartnerId).SetSerializer(new GuidSerializer(BsonType.String));
            map.MapMember(emp => emp.PreferenceId).SetSerializer(new GuidSerializer(BsonType.String));

            map.MapMember(code => code.BeginDate).SetSerializer(new DateTimeSerializer(true));
            map.MapMember(code => code.EndDate).SetSerializer(new DateTimeSerializer(true));
            map.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<Customer>(map =>
        {
            map.AutoMap();
            
            map.UnmapMember(p => p.FullName);
            map.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<Preference>(map =>
        {
            map.AutoMap();

            map.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<PromoCodeCustomer>(map =>
        {
            map.AutoMap();
            
            map.MapMember(p => p.CustomerId).SetSerializer(new GuidSerializer(BsonType.String));
            map.MapMember(p => p.PromoCodeId).SetSerializer(new GuidSerializer(BsonType.String));
            map.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<CustomerPreference>(map =>
        {
            map.AutoMap();
            
            map.MapMember(p => p.CustomerId).SetSerializer(new GuidSerializer(BsonType.String));
            map.MapMember(p => p.PreferenceId).SetSerializer(new GuidSerializer(BsonType.String));
            map.SetIgnoreExtraElements(true);
        });
    }
}
