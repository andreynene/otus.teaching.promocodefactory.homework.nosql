﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Context
{
    public class MongoDataContext: MongoDbContext
    {
        public MongoDataContext(MongoConnectionSetting mongoConnectionString) : base(mongoConnectionString)
        {
            RegisterCollection<Preference>(nameof(Preferences));
            RegisterCollection<Customer>(nameof(Customers));
            RegisterCollection<PromoCode>(nameof(PromoCodes));
        }

        public IMongoCollection<PromoCode> PromoCodes => GetCollections<PromoCode>();
        public IMongoCollection<Preference> Preferences => GetCollections<Preference>();
        public IMongoCollection<Customer> Customers => GetCollections<Customer>();
    }
}