﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly Context.MongoDataContext _mongoMongoDataContext;

        public MongoDbInitializer(Context.MongoDataContext mongoMongoDataContext)
        {
            _mongoMongoDataContext = mongoMongoDataContext;
        }
        
        public void InitializeDb()
        {
            _mongoMongoDataContext.DropCollection<PromoCode>();
            _mongoMongoDataContext.DropCollection<Customer>();

            if (_mongoMongoDataContext.Count(_mongoMongoDataContext.Preferences) == 0)
                _mongoMongoDataContext.AddRangeAsync(_mongoMongoDataContext.Preferences, FakeDataFactory.Preferences);
           
            if (_mongoMongoDataContext.Count(_mongoMongoDataContext.Customers) == 0)
                _mongoMongoDataContext.AddRangeAsync(_mongoMongoDataContext.Customers, FakeDataFactory.Customers);
        }
    }
}