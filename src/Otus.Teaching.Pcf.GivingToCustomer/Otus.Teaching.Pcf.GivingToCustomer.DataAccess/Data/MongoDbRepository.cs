﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Context;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public abstract class MongoDbRepository<TEntity, TMongoDbContext> : IRepository<TEntity> where TEntity : BaseEntity where TMongoDbContext : MongoDbContext
    {
        private readonly TMongoDbContext _dataContext;
        private readonly IMongoCollection<TEntity> _mongoCollection;

        protected MongoDbRepository(TMongoDbContext dataContext)
        {
            _dataContext = dataContext;
            _mongoCollection = _dataContext.GetCollections<TEntity>();
        }

        public Task AddAsync(TEntity entity) => 
            _mongoCollection.InsertOneAsync(entity);

        public Task UpdateAsync(TEntity entity)
        {
            return _mongoCollection.ReplaceOneAsync(item => item.Id == entity.Id, entity,
                new ReplaceOptions
                {
                    IsUpsert = true
                });
        }

        public Task DeleteAsync(TEntity entity) => 
            _mongoCollection.DeleteOneAsync(item => item.Id == entity.Id);

        public Task<IEnumerable<TEntity>> GetAllAsync() => 
            _dataContext.GetAllAsync(_mongoCollection);

        public Task<TEntity> GetByIdAsync(Guid id) => 
            _dataContext.GetByIdAsync(_mongoCollection, id.ToString());

        public Task<TEntity> GetFirstWhere(Expression<Func<TEntity, bool>> predicate) => 
            _dataContext.GetFirstWhereAsync(_mongoCollection, predicate);

        public Task<IEnumerable<TEntity>> GetRangeByIdsAsync(List<Guid> ids) => 
            _dataContext.GetRangeByIdsAsync(_mongoCollection, ids.Select(x => x.ToString()).ToArray());

        public Task<IEnumerable<TEntity>> GetWhere(Expression<Func<TEntity, bool>> predicate) => 
            _dataContext.GetWhereAsync(_mongoCollection, predicate);
    }
}