﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Context;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoRepository<T> : MongoDbRepository<T, MongoDataContext> where T : BaseEntity
    {
        public MongoRepository(MongoDataContext mongoDataContext) : base(mongoDataContext)
        {
        }
    }
}