namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoConnectionSetting
    {
        public string ConnectionString { get; set; }
        public string DbName { get; set; }
    }
}